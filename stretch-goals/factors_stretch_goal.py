def factors(number):
    # ==============
    # Your code here
    factors = []
    i = 2
    while i < number:
        if (number % i) == 0:
            factors.append(i)
        i += 1
    if len(factors) == 0:
        return str(number) + " is a prime number"
    else: 
        return factors    
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
