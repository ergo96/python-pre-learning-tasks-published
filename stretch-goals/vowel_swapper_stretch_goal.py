def vowel_swapper(string):
    # ==============
    # Your code here
    swap_dict = {
        "a":"4",
        "e":"3",
        "i":"!",
        "o":"ooo",
        "u":"|_|"
    }
    for x in swap_dict:
        string_cf = string.casefold()
        a = string_cf.find(x)
        if a != -1:
            a = string_cf.find(x, a+1)
        if a != -1:
            if string[a] == "O":
                string = string[:a] + "000" + string[a+1:]
            else:
                string = string[:a] + swap_dict[x] + string[a+1:] 
    return string
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
